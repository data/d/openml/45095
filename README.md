# OpenML dataset: Lymphoma-9

https://www.openml.org/d/45095

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Diffuse large b-cell lymphoma (Lymphoma 9 classes, DLBCL) dataset**

**Authors**: A. Alizadeh, M. Eisen, R. Davis, C. Ma, I. Lossos, A. Rosenwald, J. Boldrick, H. Sabet, T. Tran, X. Yu, et al

**Please cite**: ([URL](https://www.nature.com/articles/35000501)): A. Alizadeh, M. Eisen, R. Davis, C. Ma, I. Lossos, A. Rosenwald, J. Boldrick, H. Sabet, T. Tran, X. Yu, et al, Distinct types of diffuse large b-cell lymphoma identified by gene expression profiling, Nature 403 (6769) (2000) 503-511

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45095) of an [OpenML dataset](https://www.openml.org/d/45095). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45095/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45095/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45095/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

